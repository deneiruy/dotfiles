#!/bin/bash

rofi -config /usr/share/rofi/themes/custom.rasi -width 55 -show-icons -font "Poppins 12" -lines 10 -columns 2 -modi "drun" -display-drun "Run: " -window-format {t} -show drun -monitor -1
