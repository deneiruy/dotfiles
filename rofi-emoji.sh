#!/bin/bash

rofi -config /usr/share/rofi/themes/custom.rasi -width 49 -show-icons -font "Poppins 14" -lines 10 -columns 2 -modi "emoji" -show emoji -monitor -4 -display-emoji "Emoji: "
